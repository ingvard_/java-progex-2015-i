# Описание курса #

Формируется.  
Литература по алгоритмам: [algorithms:Dasgupta](https://bitbucket.org/ingvard_/java-progex-2015-i/downloads/draft.pdf)  
Литература по java: [Thinking_in_Java_(4th_Edition)](https://bitbucket.org/ingvard_/java-progex-2015-i/downloads/Thinking_in_Java_(4th_Edition)_RUS.pdf)  

Сайт для практики в алгоритмах: http://algo.rosalind.info/
## Рейтинг студентов ##
| Фио  |Занятие 1 |   |   |   |
|---|---|---|---|---|
| 1. Кутрюмова Галина  |  +1 |   |   |   |
| 2. Артемий Пестрецов  | +1   |   |   |   |
| 3. Роман Голышев |  +1 |   |   |   |
| 4. Илья Онищенко  |  +1 |   |   |   |
| 5. Игоръ Дроздов  |   |   |   |   |
| 6. Игорь Песчинский |  +1  |   |   |   |
| 7. Дмитрий Перец   |   |   |   |   |
| 8. Костя Чаркин  | +1 |   |   |   |
| 9. Кирилл Алексеенко   |  +1 |   |   |   |

## Проект ##
### К 8.05.15 ###
1. Spring  
  * IoC / DI  
  * Bean   
2. Заменить, MongoDBConnetion на Spring Data Mongodb.  
3. Убрать обработчики картинок в отдельные классы сервисы.  
4. Изучаем JUnit, Mockito.  
*** Структурирования проекта ***   
1. property-placeholder  


## Темы ##
### Занятие 1 ###

1. [Введение в Java](https://compscicenter.ru/courses/java/2014-autumn/classes/771/)  **(60 минут)**  
 * История и эволюция языка  
 * Ключевые особенности   
 * Hello World  
 * Инструменты разработчика: Java Development Kit  
 * Автоматизация сборки при помощи Maven  
2. [Базовый синтаксис Java](https://compscicenter.ru/courses/java/2014-autumn/classes/772/)  **(80 минут)**  
  Система типов:  
   * Примитивные типы, преобразование типов.  
   * Ссылочные типы. Массивы и строки.  
  Управляющие конструкции:  
   * Условные операторы (if, switch).  
   * Циклы (for, while, do).  
   * Операторы break и continue, метки.  
3. algorithms:Dasgupta. Пролог - ср 7..12. ** (45 минут) **   
 * O-символика
4. [HelloWorld](https://github.com/vlastachu/java_lessons/tree/master/Day-1-basic-operators-control-flow-constructs) ** (1 час) **    
* [HelloWorld](https://github.com/vlastachu/java_lessons/tree/master/Day-1-basic-operators-control-flow-constructs#helloworld), javac, java, [Объяснение исходного кода](https://github.com/vlastachu/java_lessons/tree/master/Day-1-basic-operators-control-flow-constructs#Объяснение-исходного-кода);  
   * [Пакеты](https://github.com/vlastachu/java_lessons/tree/master/Day-1-basic-operators-control-flow-constructs#Пакеты);   
   * [Числа и арифметические операторы](https://github.com/vlastachu/java_lessons/tree/master/Day-1-basic-operators-control-flow-constructs#Числа-и-арифметические-операторы) - примитивные типы, бинарные операторы, локальные переменные, значения по умолчанию, инкремент и декремент, [Таблица бинарных операторов](https://github.com/vlastachu/java_lessons/tree/master/Day-1-basic-operators-control-flow-constructs#Таблица-бинарных-операторов);   
   * [Ветвление (условный переход)](https://github.com/vlastachu/java_lessons/tree/master/Day-1-basic-operators-control-flow-constructs#Управляющие-конструкции-ветвление) - [Пример конструкции `if else`](https://github.com/vlastachu/java_lessons/tree/master/Day-1-basic-operators-control-flow-constructs#Пример-ветвления-решение-квадратного-уравнения), [конструкция `switch`](https://github.com/vlastachu/java_lessons/tree/master/Day-1-basic-operators-control-flow-constructs#Конструкция-switch), [тернарный оператор `?:`](https://github.com/vlastachu/java_lessons/tree/master/Day-1-basic-operators-control-flow-constructs#тернарный-оператор);  
   * [`&` и `&&`](https://github.com/vlastachu/java_lessons/tree/master/Day-1-basic-operators-control-flow-constructs#Отличие--от--в-условии);  
   * [Циклы](https://github.com/vlastachu/java_lessons/tree/master/Day-1-basic-operators-control-flow-constructs#Циклы);  
5. [Концепции языка Java](https://github.com/vlastachu/java_lessons/tree/master/Day-2-Conceptions) ** (30 минут) **  
* [Виртуальная машина Java](https://github.com/vlastachu/java_lessons/tree/master/Day-2-Conceptions#Виртуальная-машина-java) - что такое интерпритируемые языки, что такое jre, jdk, разные редакции java;  
   * [Управление памятью](https://github.com/vlastachu/java_lessons/tree/master/Day-2-Conceptions#Управление-памятью) - ручное управления памятью, автоматическое (shared pointer) и сборка мусора;   
   * [Язык java](https://github.com/vlastachu/java_lessons/tree/master/Day-2-Conceptions#Язык-java);  
   * [Типизация](https://github.com/vlastachu/java_lessons/tree/master/Day-2-Conceptions#Типизация);  
   * [ООП](https://github.com/vlastachu/java_lessons/tree/master/Day-2-Conceptions#ООП);  
   * [Классы](https://github.com/vlastachu/java_lessons/tree/master/Day-2-Conceptions#Классы)  
   * [Примеры встроенных объектов](https://github.com/vlastachu/java_lessons/tree/master/Day-2-Conceptions#Примеры-встроенных-объектов) - [Массивы](https://github.com/vlastachu/java_lessons/tree/master/Day-2-Conceptions#Массивы), [Многомерные массивы](https://github.com/vlastachu/java_lessons/tree/master/Day-2-Conceptions#Многомерные-массивы), [Строки](https://github.com/vlastachu/java_lessons/tree/master/Day-2-Conceptions#Строки);        
6. [Функции](https://github.com/vlastachu/java_lessons/tree/master/Day-3-functions) ** (20 минут) **   
 * [Полиморфизм](https://github.com/vlastachu/java_lessons/tree/master/Day-3-functions#Полиморфизм);  
   * [Рекурсия](https://github.com/vlastachu/java_lessons/tree/master/Day-3-functions#Рекурсия);  
   * [Переменное число параметров](https://github.com/vlastachu/java_lessons/tree/master/Day-3-functions#Переменное-число-параметров);  
   * [Пример: нахождение корня числа](https://github.com/vlastachu/java_lessons/tree/master/Day-3-functions#Пример-нахождение-корня-числа);  
   * [Java 8: лямбда функции.](https://github.com/vlastachu/java_lessons/tree/master/Day-3-functions#java-8-лямбда-функции);    
7. [ООП](https://github.com/vlastachu/java_lessons/tree/master/Day-4-OOP) ** (35 минут) **  
 * [Объекты](https://github.com/vlastachu/java_lessons/tree/master/Day-4-OOP#Объекты);  
   * [autoboxing](https://github.com/vlastachu/java_lessons/tree/master/Day-4-OOP#autoboxing);  
   * [пример cube](https://github.com/vlastachu/java_lessons/tree/master/Day-4-OOP#пример-cube);  
   * [Спецификаторы доступа](https://github.com/vlastachu/java_lessons/tree/master/Day-4-OOP#Спецификаторы-доступа);  
   * [static](https://github.com/vlastachu/java_lessons/tree/master/Day-4-OOP#static);   
   * [Ключевое слово this](https://github.com/vlastachu/java_lessons/tree/master/Day-4-OOP#Ключевое-слово-this);  
   * [final (инициализация)](https://github.com/vlastachu/java_lessons/tree/master/Day-4-OOP#final-инициализация);  
   * [Наследование](https://github.com/vlastachu/java_lessons/tree/master/Day-4-OOP#Наследование) - [Зачем нам может пригодиться наследование?](https://github.com/vlastachu/java_lessons/tree/master/Day-4-OOP#Зачем-нам-может-пригодиться-наследование);  
Итого: ***330 минут.***

[Вопросы для контроля, блок 1](https://bitbucket.org/ingvard_/java-progex-2015-i/wiki/%D0%92%D0%BE%D0%BF%D1%80%D0%BE%D1%81%D1%8B%20%D0%B4%D0%BB%D1%8F%20%D1%81%D0%B0%D0%BC%D0%BE%D0%BA%D0%BE%D0%BD%D1%82%D1%80%D0%BE%D0%BB%D1%8F,%20%D0%B1%D0%BB%D0%BE%D0%BA%201)

*** Задания ***  
**Реализовать следующие алгоритмы на java:**  
1. Fibonacci Numbers (10-20) минут.  
2. Binary Search (10-20) минут.  
3. Insertion Sort (10-20) минут.  
4. Merge Two Sorted Arrays (15-25) минут.  
5. Majority Element  O(n) (30-50) минут.     
Итого: 2 часа.

### Занятие 2 ###
1. [Объекты, классы и пакеты в Java](https://compscicenter.ru/courses/java/2014-autumn/classes/773/)  
   * Основы ООП.  
   * Класс, интерфейс, перечисление.  
   * Аннотации.  
   * Модификаторы доступа.  
   * Вложенные классы.  
   * Наследование.  
   * Пакеты.  
2. [Обработка ошибок, исключения, отладка](https://compscicenter.ru/courses/java/2014-autumn/classes/776/)  
   * Обзор подходов к обработке ошибок.  
   * Исключения, типы исключений.  
   * Конструкции try/catch/finally, try-with-resources, multicatch.  
   * Java Logging API.  
   * Использование отладчика. 

[Вопросы для контроля, блок 2](https://bitbucket.org/ingvard_/java-progex-2015-i/wiki/%D0%92%D0%BE%D0%BF%D1%80%D0%BE%D1%81%D1%8B%20%D0%B4%D0%BB%D1%8F%20%D1%81%D0%B0%D0%BC%D0%BE%D0%BA%D0%BE%D0%BD%D1%82%D1%80%D0%BE%D0%BB%D1%8F,%20%D0%B1%D0%BB%D0%BE%D0%BA%202)

## Занятие 3 ##
[Collections Framework и Generics](https://compscicenter.ru/courses/java/2014-autumn/classes/825/)
[Java-классы: взгляд изнутри](https://compscicenter.ru/courses/java/2014-autumn/classes/828/)
## Дополнительно ## 
[Static in java](http://info.javarush.ru/translation/2014/04/15/10-%D0%B7%D0%B0%D0%BC%D0%B5%D1%82%D0%BE%D0%BA-%D0%BE-%D0%BC%D0%BE%D0%B4%D0%B8%D1%84%D0%B8%D0%BA%D0%B0%D1%82%D0%BE%D1%80%D0%B5-Static-%D0%B2-Java.html)

[Servlet, Introduction to Web](http://www.studytonight.com/servlet/introduction-to-web.php)  
[Introduction to JDBC](http://www.studytonight.com/java/introduction-to-jdbc.php)  
[Hibernate](http://www.tutorialspoint.com/hibernate/)    
[High Performance Hibernate Tutorial](http://vladmihalcea.com/tutorials/hibernate/)    
toplink  
JSF  

[Spring Security – Behind the scenes](http://www.javacodegeeks.com/2013/11/spring-security-behind-the-scenes.html)  
[The Ins and Outs of Token Based Authentication](https://scotch.io/tutorials/the-ins-and-outs-of-token-based-authentication)  
[The Anatomy of a JSON Web Token](https://scotch.io/tutorials/the-anatomy-of-a-json-web-token)  
http://www.youtube.com/watch?v=m5eYeWWaViw
## Сроки ##
Занятие 1 - до 4 апреля.  
Занятие 2 - до 11 апреля.  
Занятие 3 - до 18 апреля.  
Занятие 4 - до 25 апреля.  
Занятие 5 - до 2 мая.